delete from err_list_s where Languageid = 6

Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (200,N'システムエラー：VRM管理者に問い合わせこのエラーコードをお知らせください。',N'システムエラー：VRM管理者に問い合わせこのエラーコードをお知らせください。','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (201,N'ユーザーIDがありません。操作が中止されたかユーザーIDが提供されません。',N'ユーザーIDがありません。操作が中止されたかユーザーIDが提供されません。','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (202,N'会議名を入力してください。',N'会議名を入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (203,N'有効な日付と時間を入力してください。',N'有効な日付と時間を入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (205,N'ユーザーはVRMに登録されていません。',N'ユーザーはVRMに登録されていません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (206,N'固有のログイン名を入力してください。',N'固有のログイン名を入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (207,N'ログイン名がパスワードが無効です。再度お試しください。',N'ログイン名がパスワードが無効です。再度お試しください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (208,N'あなたのアカウントは何度か無効なパスワードが試行されたためロックされました。VRM管理者にお問い合わせください。',N'あなたのアカウントは何度か無効なパスワードが試行されたためロックされました。VRM管理者にお問い合わせください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (209,N'有効なEメールアドレスを入力してください。',N'有効なEメールアドレスを入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (210,N'IPもしくはT1ポートが足りません。',N'IPもしくはT1ポートが足りません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (211,N'Mux T1ポートが足りません。',N'Mux T1ポートが足りません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (212,N'固有の会議名を入力してください。',N'固有の会議名を入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (213,N'T1ポートが足りません。',N'T1ポートが足りません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (214,N'オーディオカードが足りません。',N'オーディオカードが足りません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (215,N'ビデオカードが足りません。',N'ビデオカードが足りません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (216,N'リソース計算に複雑なエラーが発生しました',N'リソース計算に複雑なエラーが発生しました','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (217,N'固有のグループ名を入力してください。',N'固有のグループ名を入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (218,N'あなたのアカウントバランスはこの会議に不十分です。VRM管理者に問い合わせ、このエラーコードを提供してください。',N'あなたのアカウントバランスはこの会議に不十分です。VRM管理者に問い合わせ、このエラーコードを提供してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (219,N'名前、姓、またはEメールアドレスを入力してください。',N'名前、姓、またはEメールアドレスを入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (220,N'ユーザー名、ログイン、Ｅメールが重複しています。確かめてから再度入力してください。',N'ユーザー名、ログイン、Ｅメールが重複しています。確かめてから再度入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (221,N'会議は無事に作成されました。 "新規ユーザー"で重複したEメールの参加者<email address>は招待されていません -- 既にVRMのデータベースに登録されているユーザーが招待されました。',N'会議は無事に作成されました。 "新規ユーザー"で重複したEメールの参加者<email address>は招待されていません -- 既にVRMのデータベースに登録されているユーザーが招待されました。','I',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (222,N'固有のテンプレート名を入力してください。',N'固有のテンプレート名を入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (223,N'有効な会議開始日時を入力してください。',N'有効な会議開始日時を入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (224,N'スーパー管理者機能のみです。VRM管理者に追加情報をお問い合わせください。',N'スーパー管理者機能のみです。VRM管理者に追加情報をお問い合わせください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (225,N'ログインする前に会議招待を承認してください。',N'ログインする前に会議招待を承認してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (226,N'この特定の会議へのシステムリソースが不十分です。会議に含まれている参加者または部屋の数を減らしてみてください。さらにサポートが必要であればVRM管理者に問い合わせこのエラーコードを提供してください。',N'この特定の会議へのシステムリソースが不十分です。会議に含まれている参加者または部屋の数を減らしてみてください。さらにサポートが必要であればVRM管理者に問い合わせこのエラーコードを提供してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (227,N'有効なログインかパスワードを入力してください。',N'有効なログインかパスワードを入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (228,N'定例会議設定をチェックして再入力してください。',N'定例会議設定をチェックして再入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (229,N'あなたはこの機能に許可されていません。さらなるサポートにはmyVRM管理者に連絡しこのエラーコードを提示してください。',N'あなたはこの機能に許可されていません。さらなるサポートにはmyVRM管理者に連絡しこのエラーコードを提示してください。','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (230,N'あなたのアカウントはこの会議を設定するには不十分です。myVRMにお問い合わせください。',N'あなたのアカウントはこの会議を設定するには不十分です。myVRMにお問い合わせください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (233,N'システムエラー。ブラウザを閉じ再スタートさせてからVRMに再びログインしてアプリケーションをリセットしてください。',N'システムエラー。ブラウザを閉じ再スタートさせてからVRMに再びログインしてアプリケーションをリセットしてください。','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (234,N'あなたの検索基準にマッチするグループはありません。',N'あなたの検索基準にマッチするグループはありません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (237,N'固有のブリッジ名を入力してください。',N'固有のブリッジ名を入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (238,N'請求書の間違い。VRM管理者に問い合わせこのエラーコードを提示してください。',N'請求書の間違い。VRM管理者に問い合わせこのエラーコードを提示してください。','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (240,N'この特定の会議のためのシステムリソースが不十分です。ブリッジに使用可能なISDN電話番号がみつかりませんでした。さらなるサポートにはVRM管理者に問い合わせこのエラーコードを提示してください。',N'この特定の会議のためのシステムリソースが不十分です。ブリッジに使用可能なISDN電話番号がみつかりませんでした。さらなるサポートにはVRM管理者に問い合わせこのエラーコードを提示してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (242,N'リスエストされた会議は終了したか存在しないかです。',N'リスエストされた会議は終了したか存在しないかです。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (243,N'リクエストされた会議は主催者か管理者によって削除されました。',N'リクエストされた会議は主催者か管理者によって削除されました。','I',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (244,N'ポイント・ツー・ポイント会議は管理者によって無効になりました。会議を設定するためにほかのセクションから選んでください。',N'ポイント・ツー・ポイント会議は管理者によって無効になりました。会議を設定するためにほかのセクションから選んでください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (250,N'無効なセキュリティーキーまたは時間切れです。さらなるサポートにはVRM管理者にお問い合わせください。',N'無効なセキュリティーキーまたは時間切れです。さらなるサポートにはVRM管理者にお問い合わせください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (253,N'VRMシステムは選択した日時では使用不可です。VRMシステム利用可能日時を調べ再試行してください。',N'VRMシステムは選択した日時では使用不可です。VRMシステム利用可能日時を調べ再試行してください。','C',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (254,N'あなたのアカウントはロックされました。サポートにはVRM管理者にお問い合わせください。',N'あなたのアカウントはロックされました。サポートにはVRM管理者にお問い合わせください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (255,N'リクエストされた会議は承認待ちです。VRM管理者に今後のサポートをお問い合わせください。',N'リクエストされた会議は承認待ちです。VRM管理者に今後のサポートをお問い合わせください。','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (256,N'リクエスト名は既に使われています。他の名前を選んでください。',N'リクエスト名は既に使われています。他の名前を選んでください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (257,N'この市にリクエストされた建物名は既に使われています。他の建物名を選んでください。',N'この市にリクエストされた建物名は既に使われています。他の建物名を選んでください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (258,N'リクエストされた市の名前は既に使われています。他の名前を選んでください。',N'リクエストされた市の名前は既に使われています。他の名前を選んでください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (259,N'リクエストされた団体は既に会議に存在しています。',N'リクエストされた団体は既に会議に存在しています。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (260,N'リクエストされたブリッジ名は既に他のブリッジに割り当てられています。',N'リクエストされたブリッジ名は既に他のブリッジに割り当てられています。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (262,N'一つかそれ以上の選択した部屋は提携しているエンドポイントがありません。',N'一つかそれ以上の選択した部屋は提携しているエンドポイントがありません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (263,N'あなたのVRMアカウントは期限切れでロックされました。地域のVRM管理者に更なるサポートをお問い合わせください。',N'あなたのVRMアカウントは期限切れでロックされました。地域のVRM管理者に更なるサポートをお問い合わせください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (264,N'あなたのVRMアカウントはあと %で期限切れです。VRM管理者に更なるサポートをお問い合わせください。あなたのアカウントは{0}日で期限切れになります。',N'あなたのVRMアカウントはあと %で期限切れです。VRM管理者に更なるサポートをお問い合わせください。あなたのアカウントは{0}日で期限切れになります。','M',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (302,N'無効なセキュリティーキーまたは時間切れです。',N'無効なセキュリティーキーまたは時間切れです。','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (305,N'SQLを実行できません。テキストにエラーがあります。',N'SQLを実行できません。テキストにエラーがあります。','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (306,N'DBを読み込めません。',N'DBを読み込めません。','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (307,N'無効なアカウント有効期限です。ライセンスを確認してください。',N'無効なアカウント有効期限です。ライセンスを確認してください。','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (308,N'会議は削除できません。期限切れかプログラムが終了したかどちらかです。',N'会議は削除できません。期限切れかプログラムが終了したかどちらかです。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (309,N'ユーザーアカウント情報が見つからないかデータベースで破損したかです',N'ユーザーアカウント情報が見つからないかデータベースで破損したかです','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (310,N'ユーザー操作中にエラーが起こりました。ユーザーは使用中です。削除できません！',N'ユーザー操作中にエラーが起こりました。ユーザーは使用中です。削除できません！','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (311,N'内部エラーが発生しました－エラーコード311',N'内部エラーが発生しました－エラーコード311','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (312,N'特定の会議のタイムゾーンデータエラー１　',N'特定の会議のタイムゾーンデータエラー１　','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (313,N'タイムゾーン表エラー',N'タイムゾーン表エラー','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (314,N'クエリーエラー<データベースクエリー>',N'クエリーエラー<データベースクエリー>','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (315,N'ユーザーエラーを取り換える',N'ユーザーエラーを取り換える','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (316,N'クエリーできません[ユーザー]',N'クエリーできません[ユーザー]','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (317,N'バウンディッドオペレーターはクエリーできません。',N'バウンディッドオペレーターはクエリーできません。','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (318,N'食事メニューから食事ID、食事名を選ぶ',N'食事メニューから食事ID、食事名を選ぶ','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (319,N'グループリストをフェッチする',N'グループリストをフェッチする','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (320,N'グループ参加者からクエリーカウント',N'グループ参加者からクエリーカウント','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (321,N'グループ参加者からのクエリー, [ユーザー]',N'グループ参加者からのクエリー, [ユーザー]','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (323,N'会議グループ挿入不可',N'会議グループ挿入不可','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (325,N'エラー削除',N'エラー削除','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (326,N'会議室挿入不可',N'会議室挿入不可','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (327,N'会議室に他の場所を挿入不可',N'会議室に他の場所を挿入不可','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (328,N'クエリー不可[会議室]',N'クエリー不可[会議室].','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (330,N'場所をフェッチする',N'場所をフェッチする','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (334,N'オペレーターエラー選択',N'オペレーターエラー選択','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (400,N'XMLバリデーション・エラー',N'XMLバリデーション・エラー','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (402,N'最大限のグループを超えました',N'最大限のグループを超えました','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (403,N'最大限のテンプレートを超えました',N'最大限のテンプレートを超えました','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (404,N'ライセンス読み込みエラー',N'ライセンス読み込みエラー','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (406,N'部門は使用中です。削除できません！',N'部門は使用中です。削除できません！','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (407,N'ライセンスチェックに失敗しました。近くの管理者かmyVRMにさらなるサポートのためにお問い合わせください。  ',N'ライセンスチェックに失敗しました。近くの管理者かmyVRMにさらなるサポートのためにお問い合わせください。 ','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (408,N'無効な日付幅が選ばれました',N'無効な日付幅が選ばれました','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (410,N'会議作成エラー。分割した部屋は非ビデオにしかできません。',N'会議作成エラー。分割した部屋は非ビデオにしかできません','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (411,N'MCUは削除できません。全ての付属するリソースを消して再試行してください。',N'MCUは削除できません。全ての付属するリソースを消して再試行してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (412,N'重複した部門名。作成/編集できません！',N'重複した部門名。作成/編集できません！','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (413,N'エンドポイント名は既に存在します。他の名前を選んでください。',N'エンドポイント名は既に存在します。他の名前を選んでください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (414,N'リンクに失敗しました。トランザクションロールバック！ ',N'リンクに失敗しました。トランザクションロールバック！','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (415,N'重複したグループ名',N'グループ名','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (416,N'コマンドはサポートされていません。',N'コマンドはサポートされていません。','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (417,N'階層は使用中なので削除できません。',N'階層は使用中なので削除できません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (418,N'エラーエンドポイントはブリッジを割り当てられなければいけません。',N'エラーエンドポイントはブリッジを割り当てられなければいけません。','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (419,N'会議は終わりました。',N'会議は終わりました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (420,N'無効なカテゴリータイプ',N'ワークオーダー ','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (422,N'無効な操作',N'無効な操作','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (423,N'無効な組織ID',N'無効な組織ID','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (424,N'無効なイメージまたはIDが見つかりません',N'無効なイメージまたはIDが見つかりません','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (425,N'サイトは現在有効でありません。VRM管理者にお問い合わせください。',N'サイトは現在有効でありません。VRM管理者にお問い合わせください。','S',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (427,N'名前は階層１に既に存在します。',N'名前は階層１に既に存在します。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (428,N'名前は階層２に既に存在します。',N'名前は階層２に既に存在します。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (429,N'テンプレートのライセンスが期限切れです。VRM管理者にお問い合わせください。',N'テンプレートのライセンスが期限切れです。VRM管理者にお問い合わせください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (430,N'管理者ユーザーが削除されました。',N'管理者ユーザーが削除されました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (431,N'レポート作成中にエラーが生じました',N'レポート作成中にエラーが生じました','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (432,N'定例会議は少なくとも２回会議を含んでください。会議を送信する前に繰り返しパターンを修正してください。',N'定例会議は少なくとも２回会議を含んでください。会議を送信する前に繰り返しパターンを修正してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (433,N'部屋を選んでください。',N'部屋を選んでください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (434,N'エンドポイント名は既に存在します。',N'エンドポイント名は既に存在します。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (435,N'エラーMCU使用が計算できません',N'エラーMCU使用が計算できません','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (436,N'エラーベンダーがみつかりません',N'エラーベンダーがみつかりません','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (437,N'このユーザーから承認待ちの会議があります。このユーザーを削除する前にリスト>承認待ちを見る>で会議を承認してください。',N'このユーザーから承認待ちの会議があります。このユーザーを削除する前にリスト>承認待ちを見る>で会議を承認してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (438,N'エラー　テンプレートは使用中です。削除できません。',N'エラー　テンプレートは使用中です。削除できません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (439,N'組織設定を更新中にエラーが生じました。',N'組織設定を更新中にエラーが生じました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (440,N'ユーザーの承認カウンターの更新中にエラーが生じました。',N'ユーザーの承認カウンターの更新中にエラーが生じました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (441,N'組織のシステム承認者を更新中にエラーが生じました。',N'組織のシステム承認者を更新中にエラーが生じました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (442,N'組織のテクニカル詳細を更新中にエラーが生じました。',N'組織のテクニカル詳細を更新中にエラーが生じました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (443,N'組織詳細を更新中にエラーが生じました。',N'組織詳細を更新中にエラーが生じました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (444,N'サイト管理者のみが組織を作成/編集できます。',N'サイト管理者のみが組織を作成/編集できます。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (445,N'組織の限度が上回る',N'組織の限度が上回る','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (446,N'組織タイトルが既に存在します。',N'組織タイトルが既に存在します。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (447,N'組織のデフォルトデータ作成中にエラーが生じました。',N'組織のデフォルトデータ作成中にエラーが生じました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (448,N'組織のデフォルトテクニカルデータ作成中にエラーが生じました。',N'組織のデフォルトテクニカルデータ作成中にエラーが生じました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (449,N'組織のデフォルトカスタムアトリビュート作成中にエラーが発生しました。',N'組織のデフォルトカスタムアトリビュート作成中にエラーが発生しました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (450,N'ビデオルームの限度はVRMのライセンスを超過します。',N'組織リソース限度の更新中にエラーが生じました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (451,N'ファシリティーモジュールが使用できません。ライセンスカウントが超過しました。',N'ファシリティーモジュールが使用できません。ライセンスカウントが超過しました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (452,N'ケータリングモジュールが使用できません。ライセンスカウントが超過しました。',N'ケータリングモジュールが使用できません。ライセンスカウントが超過しました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (453,N'ハウスキーピングモジュールが使用できません。ライセンスカウントが超過しました。',N'ハウスキーピングモジュールが使用できません。ライセンスカウントが超過しました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (454,N'ビデオルームの限度はVRMのライセンスを超過します。',N'APIモジュールが使用できません。ライセンスカウントが超過しました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (455,N'ビデオルームの限度はVRMのライセンスを超過します。',N'ビデオルームの限度はVRMのライセンスを超過します。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (456,N'非ビデオルームの限度はVRMのライセンスを超過します。',N'非ビデオルームの限度はVRMのライセンスを超過します。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (457,N'MCUの限度はVRMのライセンスを超過します。',N'MCUの限度はVRMのライセンスを超過します。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (458,N'エンドポイントの限度はVRMライセンスを超過します。',N'エンドポイントの限度はVRMライセンスを超過します。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (459,N'ユーザー限度はVRMライセンスを超過します。',N'ユーザー限度はVRMライセンスを超過します。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (460,N'エクスチェンジユーザー限度はVRMライセンスを超過します。',N'エクスチェンジユーザー限度はVRMライセンスを超過します。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (461,N'ドミノユーザー限度はVRMライセンスを超過します。',N'ドミノユーザー限度はVRMライセンスを超過します。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (463,N'カウントを減らすためビデオルームを無効にしてください。もっと有効なビデオルームがあるためです。',N'カウントを減らすためビデオルームを無効にしてください。もっと有効なビデオルームがあるためです。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (464,N'カウントを減らすため非ビデオルームを無効にしてください。もっと有効な非ビデオルームがあるためです。',N'カウントを減らすため非ビデオルームを無効にしてください。もっと有効な非ビデオルームがあるためです。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (465,N'カウントを減らすためMCUを削除してください。もっと有効なMCUがあるためです。',N'カウントを減らすためMCUを削除してください。もっと有効なMCUがあるためです。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (466,N'カウントを減らすためエンドポイントを削除してください。もっと有効なエンドポイントがあるためです。',N'カウントを減らすためエンドポイントを削除してください。もっと有効なエンドポイントがあるためです。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (467,N'カウントを減らすためユーザーを削除してください。もっと多くのアクティブユーザーがいるためです。',N'カウントを減らすためユーザーを削除してください。もっと多くのアクティブユーザーがいるためです。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (468,N'デフォルト組織は削除できません。',N'デフォルト組織は削除できません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (469,N'組織はアクティブユーザーリストを持っています。組織を削除する前にユーザーを削除してください。',N'組織はアクティブユーザーリストを持っています。組織を削除する前にユーザーを削除してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (470,N'組織はルームリストを持っています。組織を削除する前に部屋を削除してください。',N'組織はルームリストを持っています。組織を削除する前に部屋を削除してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (471,N'組織はMCUリストを含んでいます。組織を削除する前にMCUを削除してください。',N'組織はMCUリストを含んでいます。組織を削除する前にMCUを削除してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (472,N'組織を削除中にエラーが生じました。',N'組織を削除中にエラーが生じました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (473,N'レポートは削除できません。',N'レポートは削除できません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (474,N'レポートは存在しません。',N'レポートは存在しません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (475,N'操作は成功しました！',N'操作は成功しました！','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (476,N'LDAPコンバージョンにエラーが発生しました。ユーザーテンプレート記録がありません。',N'LDAPコンバージョンにエラーが発生しました。ユーザーテンプレート記録がありません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (477,N'LDAPコンバージョンにエラーが発生しました。ユーザーロール記録がありません。',N'LDAPコンバージョンにエラーが発生しました。ユーザーロール記録がありません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (478,N'ユーザー名/Eメールにエラーが発生しました。追加できません',N'ユーザー名/Eメールにエラーが発生しました。追加できません','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (480,N'エラー日時は変換できません',N'エラー日時は変換できません','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (481,N'削除できません！メニューは下記のワークオーダーで使用されています：',N'削除できません！メニューは下記のワークオーダーで使用されています：','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (482,N'カウントを減らすためにエクスチェンジユーザーを削除してください。もっと活動的なエクスチェンジユーザーがいます。',N'カウントを減らすためにエクスチェンジユーザーを削除してください。もっと活動的なエクスチェンジユーザーがいます。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (483,N'選択したカスタムオプションは会議とリンクされています。編集/削除できません。',N'選択したカスタムオプションは会議とリンクされています。編集/削除できません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (484,N'カスタムアトリビュートを削除中にエラーが発生しました。',N'カスタムアトリビュートを削除中にエラーが発生しました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (485,N'カスタムオプションのタイトルは既に存在します。',N'カスタムオプションのタイトルは既に存在します。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (486,N'カスタムオプションの最大限度は',N'カスタムオプションの最大限度は','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (487,N'エラーユーザーは管理責任者として承認されていません。インベントリは保存されていません。',N'エラーユーザーは管理責任者として承認されていません。インベントリは保存されていません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (488,N'エラー名前は既に存在します。',N'エラー名前は既に存在します。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (489,N'エラーユーザーは部屋を管理するために部門から承認されていません。',N'エラーユーザーは部屋を管理するために部門から承認されていません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (490,N'エラーインベントリルームは除外することができません。ワークオーダー承認待ちです。',N'エラーインベントリルームは除外することができません。ワークオーダー承認待ちです。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (491,N'エラーユーザーは管理責任者として承認されていません。ワークオーダーが保存されませんでした。',N'エラーユーザーは管理責任者として承認されていません。ワークオーダーが保存されませんでした。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (492,N'エラーユーザーはワークオーダーを管理するための部門承認がありません。ワークオーダーは保存されませんでした。',N'エラーユーザーはワークオーダーを管理するための部門承認がありません。ワークオーダーは保存されませんでした。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (493,N'エラー使用中のインベントリは削除できません。 ',N'エラー使用中のインベントリは削除できません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (494,N'カウントを減らすためにドミノユーザーを削除してください。さらに多くのアクティブユーザーがいます。',N'カウントを減らすためにドミノユーザーを削除してください。さらに多くのアクティブユーザーがいます。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (495,N'このMCUに関連したユーザーは {0} です。',N'このMCUに関連したユーザーは {0} です。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (496,N'このMCUに指定されたエンドポイントは {0} です。 ',N'このMCUに指定されたエンドポイントは {0} です。 ','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (497,N'会議のオーナーは会議作成中に存在しません：ファイル：',N'会議のオーナーは会議作成中に存在しません：ファイル：','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (498,N'無効なEメールアドレス',N'無効なEメールアドレス','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (499,N'必須のカスタムオプションの数値を設定してください。',N'必須のカスタムオプションの数値を設定してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (500,N'リアルタイムインベントリを計算している間にエラーが発生しました。',N'リアルタイムインベントリを計算している間にエラーが発生しました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (501,N'LDAP接続失敗しました。',N'LDAP接続失敗しました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (502,N'LDAP設定をフェッチングする間にエラーが発生しました。',N'LDAP設定をフェッチングする間にエラーが発生しました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (503,N'LDAPユーザーを承認する間にエラーが発生しました。',N'LDAPユーザーを承認する間にエラーが発生しました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (504,N'LDAP保留エリアでユーザーを比較しローディングする間にエラーが発生しました。',N'LDAP保留エリアでユーザーを比較しローィングする間にエラーが発生しました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (505,N'接続成功です。しかし適応するLDAPレコードが見つかりませんでした。',N'接続成功です。しかし適応するLDAPレコードが見つかりませんでした。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (506,N'承認失敗しました。適応するLDAPレコードが見つかりませんでした。',N'承認失敗しました。適応するLDAPレコードが見つかりませんでした。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (507,N'エンドポイントは削除できません：エンドポイントは部屋と関連しています。',N'エンドポイントは削除できません：エンドポイントは部屋と関連しています。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (508,N'エンドポイントが見つかりませんでした。エンドポイントをアップデートできません：',N'エンドポイントが見つかりませんでした。エンドポイントをアップデートできません：','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (509,N'ユーザーまたはIDAP表をアップデートできません',N'ユーザーまたはIDAP表をアップデートできません','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (511,N'ライセンスチェック失敗：組織限度を超過しました',N'ライセンスチェック失敗：組織限度を超過しました','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (512,N'ライセンスチェック失敗：部屋の限度を超過しました',N'ライセンスチェック失敗：部屋の限度を超過しました','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (513,N'ライセンスチェック失敗：ユーザー限度を超過しました',N'ライセンスチェック失敗：ユーザー限度を超過しました','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (514,N'ライセンスチェック失敗：MCUの限度を超過しました',N'ライセンスチェック失敗：MCUの限度を超過しました','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (515,N'ライセンスチェック失敗：有効期限が間違っています',N'ライセンスチェック失敗：有効期限が間違っています','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (516,N'MCUをプライベートとしてアップロードする間にエラーが発生しました。MCUは使用されています。',N'MCUをプライベートとしてアップロードする間にエラーが発生しました。MCUは使用されています。','E',6,'U')
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType) VALUES (522,N'このバージョンのmyVRMアドインはサポートされていません。',N'このバージョンのmyVRMアドインはサポートされていません。','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType) VALUES (523,N'ユーザーはエクスチェンジライセンスを持っていません。',N'ユーザーはエクスチェンジライセンスを持っていません。','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType) VALUES (524,N'ユーザーはドミノライセンスを持っていません。',N'ユーザーはドミノライセンスを持っていません。','E',6,'U');
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (525,N'ユーザーはモバイルライセンスを持っていません。',N'ユーザーはモバイルライセンスを持っていません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (526,N'モバイルユーザー限度はVRMライセンスを超過しました。',N'モバイルユーザー限度はVRMライセンスを超過しました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (527,N'カウントを減らすためにモバイルユーザーを削除してください。もっと多くの有効なモバイルユーザーがいます。',N'カウントを減らすためにモバイルユーザーを削除してください。もっと多くの有効なモバイルユーザーがいます。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (528,N'ライセンスチェック失敗：どの組織でもモバイルユーザー限度を減らしてください。',N'ライセンスチェック失敗：どの組織でもモバイルユーザー限度を減らしてください。','E',6,'U')
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)VALUES (518,N'ユーザーはスケジューラー/主催者/参加者として会議にリンクされています。削除できません。',N'ユーザーはスケジューラー/主催者/参加者として会議にリンクされています。削除できません。','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)VALUES (519,N'ユーザー操作にエラーが発生しました：ユーザーはシステム/ＭＣＵ/部屋/部門承認者として指定されています。削除できません。',N'ユーザー操作にエラーが発生しました：ユーザーはシステム/ＭＣＵ/部屋/部門承認者として指定されています。削除できません。','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)VALUES (520,N'ユーザー操作にエラーが発生しました：ユーザーはシステム/ＭＣＵ/部屋/ワークオーダー責任者として指定されています。削除できません。',N'ユーザー操作にエラーが発生しました：ユーザーはシステム/ＭＣＵ/部屋/ワークオーダー責任者として指定されています。削除できません。','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)VALUES (521,N'ユーザーは、グループ/テンプレート/部署名にリンクされています。削除することはできません！',N'ユーザーは、グループ/テンプレート/部署名にリンクされています。削除することはできません！','E',6,'U');
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (529,N'からの電子メールIDを入力してください。',N'からの電子メールIDを入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (530,N'パスワードを入力してください。',N'パスワードを入力してください。','E',6,'U')
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)VALUES (531,N'ブリッジIDは空です。',N'ブリッジIDは空です。','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)VALUES (534,N'この会議の参加者ではありませんまたは選択された会議では、渡されたか、または削除されました。',N'この会議の参加者ではありませんまたは選択された会議では、渡されたか、または削除されました。','S',6,'U');
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (536,N'15分を最小限に抑えて会議時間を入力してください。',N'15分を最小限に抑えて会議時間を入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (537,N'選択したカスタム日付インスタンスの競合。',N'選択したカスタム日付インスタンスの競合。','E',6,'U')
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType) VALUES (538,N'使用中の部屋（そのユーザによって優先ルームとして使用されているとして削除することはできません）！',N'使用中の部屋（そのユーザによって優先ルームとして使用されているとして削除することはできません）！','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)VALUES (539,N'使用中の部屋（そのユーザーテンプレートの優先室として使用されているとして削除することはできません）！',N'使用中の部屋（そのユーザーテンプレートの優先室として使用されているとして削除することはできません）！','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType) VALUES (540,N'使用中の部屋（そのWorkorderで使用されているとして削除することはできません）！',N'使用中の部屋（そのWorkorderで使用されているとして削除することはできません）！','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType) VALUES (541,N'使用中の部屋（その検索テンプレートで使用されているとして削除することはできません）！',N'使用中の部屋（その検索テンプレートで使用されているとして削除することはできません）！','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)VALUES (542,N'オーディオ/ビデオ会議は、管理者によって無効にされています。あなたのVRM管理者に連絡してください。',N'オーディオ/ビデオ会議は、管理者によって無効にされています。あなたのVRM管理者に連絡してください。','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)VALUES (543,N'オーディオのみの会議は、管理者によって無効にされています。あなたのVRM管理者に連絡してください。',N'オーディオのみの会議は、管理者によって無効にされています。あなたのVRM管理者に連絡してください。','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType) VALUES (544,N'部屋の会議は、管理者によって無効にされています。あなたのVRM管理者に連絡してください。',N'部屋の会議は、管理者によって無効にされています。あなたのVRM管理者に連絡してください。','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType) VALUES (545,N'部屋にはテンプレートにリンクされています。無効にすることはできません！', N'部屋にはテンプレートにリンクされています。無効にすることはできません！','E',6,'U')
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType) VALUES (547,N'エラーは、電子メールを作成することはできません。',N'エラーは、電子メールを作成することはできません。','E',6,'U');
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType) VALUES (546,N'少なくとも一つのアイテムが0より発注数量以上が必要です', N'少なくとも一つのアイテムが0より発注数量以上が必要です','E',6,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (548,N'いいえエンティティのオプションが見つかりません！',N'いいえエンティティのオプションが見つかりません！','E',6,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (549,N'選択したカスタムオプションは、任意の会議にリンクされていません。',N'選択したカスタムオプションは、任意の会議にリンクされていません。','E',6,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (605,N'電話番号を入力してください。',N'電話番号を入力してください。','E',6,'U');
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (606,N'いいえヘルプ依頼メールのエントリが見つかりませんでした。',N'いいえヘルプ依頼メールのエントリが見つかりませんでした。','E',6,'U');
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (510,N'部屋の競合は、いくつかの定期的なインスタンスで見つかった。部屋のみの予約をすることはできません日付/時刻やサービスの種類や部屋を変更してください',N'部屋の競合は、いくつかの定期的なインスタンスで見つかった。部屋のみの予約をすることはできません日付/時刻やサービスの種類や部屋を変更してください','C',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (479,N'エラーは、あなただけのユーザーを追加する必要がありません！',N'エラーは、あなただけのユーザーを追加する必要がありません！','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (462,N'ユーザー数の制限はドミノ、エクスチェンジおよびモバイルユーザのための包括的でなければなりません。',N'ユーザー数の制限はドミノ、エクスチェンジおよびモバイルユーザのための包括的でなければなりません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (331,N'confidどこconfuserからカウント（*）を選択します。',N'confidどこconfuserからカウント（*）を選択します。','S',6,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (426,N'要求されたメールアドレス/ログインは既に使用されている - 非アクティブ状態',N'要求されたメールアドレス/ログインは既に使用されている - 非アクティブ状態','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (231,N'ホストが前に口座を開くことができませんでした。財務表には、ユーザーのタプルはありません。',N'ホストが前に口座を開くことができませんでした。財務表には、ユーザーのタプルはありません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (241,N'部屋の競合は、部屋のみの予約をすることはできません日付/時刻やサービスの種類や部屋を変更してください ',N'部屋の競合は、部屋のみの予約をすることはできません日付/時刻やサービスの種類や部屋を変更してください','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (252,N'あなたのVRMライセンスは、この操作を許可しない',N'あなたのVRMライセンスは、この操作を許可しない','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (261,N'リクエストされたユーザー名/Eメールは既に使われています',N'リクエストされたユーザー名/Eメールは既に使われています','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (322,N'会議グループ挿入<stmt>',N'会議グループ挿入<stmt>','S',6,'S')
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (532,N'ユーザIDまたは会議IDは無効です！',N'ユーザIDまたは会議IDは無効です！','S',7,'U')
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (533,N'学科見つかりません！',N'学科見つかりません！','S',7,'U')
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (535,N'会議は、与えられた量以上の量の在庫·セットに関連付けられています！',N'会議は、与えられた量以上の量の在庫·セットに関連付けられています！','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (335,N'アドレスが重複しています。別のアドレスを入力してください。','アドレスが重複しています。別のアドレスを入力してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (550,N'お部屋は、会議にリンクされています。非アクティブ化できません。',N'お部屋は、会議にリンクされています。非アクティブ化できません。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (604,N'会議では、すでに削除または期限切れ',N'会議では、すでに削除または期限切れ','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (607,N'PCモジュールを有効にすることはできません、ライセンスカウントを超えました。', N'PCモジュールを有効にすることはできません、ライセンスカウントを超えました。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (608,N'現在会議がPCの出席者にリンクされ、さらなる支援のための管理者に連絡してください。 ', N'現在会議がPCの出席者にリンクされ、さらなる支援のための管理者に連絡してください。','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (609,N'カスタムオプションに少なくとも1つのオプションを追加してください。 ', N'カスタムオプションに少なくとも1つのオプションを追加してください。','E',6,'U')
/* *************************** FB 2390 - start *************************** */

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (610,N'ポイントツーポイント会議は、呼び出し元と呼び出し先が割り当てられている必要があります。 ',
N'ポイントツーポイント会議は、呼び出し元と呼び出し先が割り当てられている必要があります。','E',7,'U')

/* *************************** FB 2390 - end *************************** */



--FB 2261 and Fb 2440
INSERT INTO [dbo].[Err_List_S]
           ([ID]
           ,[Message]
           ,[Description]
           ,[Level]
           ,[Languageid]
           ,[ErrorType])
     VALUES
           (613
           ,'Setup time cannot be less than the MCU pre start time.'
           ,N'セットアップ時間は、MCU、事前の開始時間より小さくすることはできません。'
           ,'E'
           ,6
           ,'U')

INSERT INTO [dbo].[Err_List_S]
           ([ID]
           ,[Message]
           ,[Description]
           ,[Level]
           ,[Languageid]
           ,[ErrorType])
     VALUES
           (614
           ,'Teardown time cannot be less than the MCU pre end time.'
           ,N'分解時間は、MCU事前に終了時間未満にすることはできません。'
           ,'E'
           ,6
           ,'U')


/**FB 2426 FOR JAPANESE*******/


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (615,N'カウントを減らすために客室を削除してください。よりアクティブなゲストルームがありますように.',N'カウントを減らすために客室を削除してください。よりアクティブなゲストルームがありますように.','E',6,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (616,N'ゲストルームは、ユーザライセンス超えるごとにゲストルームの制限として作成されていない.',N'ゲストルームは、ユーザライセンス超えるごとにゲストルームの制限として作成されていない.','E',6,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (617,N'ゲストルームの制限ライ
センスを超えるようにゲストルームが作成されない.',N'ゲストルームの制限ライセンスを超えるようにゲストルームが作成されない.','E',6,'U')



Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (618,N'ゲストルームの制限は、VRMのライセンスを超えています.',N'ゲストルームの制限は、VRMのライセンスを超えています.','E',6,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (619,N'ユーザーごとにゲストルームの制限はゲストルームの制限を超えて.',N'ユーザーごとにゲストルームの制限はゲストルームの制限を超えて.','E',6,'U')


/*   ********* FB 2448 Starts ************** */
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (620, N'のみVMRルームは、この会議のために選択することができます。',
'のみVMRルームは、この会議のために選択することができます','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (621,N'唯一のVMR部屋を選択してください。',
'唯一のVMR部屋を選択してください。','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (622, N'解除VMRルームをしてください。',
'解除VMRルームをしてください。','E',6,'U')
/*   ********* FB 2448 Starts ************** */

/*   ********* FB 2426 ************** */
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (623,N'ゲストルームは、オーディオおよびビデオ会議の種類のために許可します。',N'ゲストルームは、オーディオおよびビデオ会議の種類のために許可します。','E',6,'U')


/* ************************ FB 2486 Starts (09 Aug 2012) ************************  */

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (6, 'Max Allowed Standard MCU', N'Maxは許可された標準的なMCU')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (6, 'Max Allowed Enhanced MCU', N'Maxは、強化されたMCUを許可')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (624, N'強化されたMCUの制限は、VRMのライセンスを超えています. ', N'強化されたMCUの制限は、VRMのライセンスを超えています.','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (625, N'ライセンスチェックに失敗しました:強化されたMCUの制限は標準MCUを超えています.', N'ライセンスチェックに失敗しました:強化されたMCUの制限は標準MCUを超えています..','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (626, N'強化されたMCUの制限は標準MCUを超えています.', N'強化されたMCUの制限は標準MCUを超えています.','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (551, N'再発の詳細を挿入することでエラーが発生しました。',
'Error in inserting Recurrence details.','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (552, N'AdvancedAduioのparamertersを挿入でエラーが発生しました。',
'Error in inserting AdvancedAduio paramerters.','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (553, N'ユーザーの詳細を追加でエラーが発生しました。',
'Error in adding User details.','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (554, N'P会議のカスタム属性のレコードを挿入するにエラーが発生しました。',
'Error in inserting Conference Custom Attributes records.','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (555, N'会議のメッセージオーバーレイのレコードを挿入でエラーが発生しました。',
'Error in inserting Conference Message Overlay records.','E',6,'U')

/* ************************ FB 2486 Ends (09 Aug 2012) ************************  */

/*  **************** FB 2506 Starts (2012-08-29) ****************** */
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (556, N'組織のために会議のメッセージを作成する際にエラーが発生しました。',
'Error in creating Conference Message for the organization.','E',6,'U')
/*  **************** FB 2506 Ends (2012-08-29) ****************** */

-- ****************************** FB 2501 StartMode - Starts - 13th Sep 2012 ******************************************
Insert into Err_List_S values(557,N'無効な起動モード。',N'無効な起動モード。','E',6,'U')
-- **************************** FB 2501 StartMode - Ends - 13th Sep 2012*********************************************//

--***************************** FB 2501_ Overwrite of Meeting Requestor name(27th Sep 2012)**************************--
Insert into Err_List_S values(627,N'ログインユーザIDが欠落しています。操作は中断されたり、ログインユーザIDが指定されていません。',N'ログインユーザIDが欠落しています。操作は中断されたり、ログインユーザIDが指定されていません。','E',6,'U')
--***************************** FB 2501_ Overwrite of Meeting Requestor name(27th Sep 2012)**************************--

--***************************** FB 2537 and FB 2517 - Starts (16th Oct 2012)**************************--
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (558,N'ライセンスチェックに失敗しました：エンドポイントの制限を超えました。',N'ライセンスチェックに失敗しました：エンドポイントの制限を超えました。','E',6,'U')
Insert into Err_List_S values(628,N'カウントを減らすために MCU を強化を削除してください。あるのでより積極的な MCU の強化します。',N'カウントを減らすために MCU を強化を削除してください。あるのでより積極的な MCU の強化します。','E',6,'U')
--***************************** FB 2537 and FB 2517 - Ends (16th Oct 2012)**************************--



/* ************************ FB 2539 ************************  */


Insert into Err_List_S values(629,'User not authorized as Assistant-in-charge.','User not authorized as Assistant-in-charge.','E',6,'U')

Insert into Err_List_S values(630,'User not authorized as VNOC Operator.','User not authorized as VNOC Operator.','E',6,'U')

Insert into Err_List_S values(631,'User not authorized as Guest Room Assistant-in-charge.','User not authorized as Guest Room Assistant-in-charge.','E',6,'U')

Insert into Err_List_S values(632,'User not authorized as Approver.','User not authorized as Approver.','E',6,'U')

--FB 2609
Insert into Err_List_S values(633,'Conference schedule time is less than Meet and Greet buffer.','Conference schedule time is less than Meet and Greet buffer.','E',6,'U')


--FB 2609 - 07thFeb 2013
Update Err_List_S set Message = 'We’re sorry, but the request for Meet and Greet support does not meet the minimum notice requirement. Please choose another time for the conference, or deselect the Meet and Greet request to complete this conference schedule.', 
Description = 'We’re sorry, but the request for Meet and Greet support does not meet the minimum notice requirement. Please choose another time for the conference, or deselect the Meet and Greet request to complete this conference schedule.' where ID = 633 and Languageid = 6

--FB 2632 - 07thFeb 2013
Insert into Err_List_S values(634,'Please select Dedicated VNOC Operator.','Please select Dedicated VNOC Operator.','E',6,'U')

--FB 2626 - 11thFeb 2013

Insert into Err_List_S values(635,'The item is linked with a Inventory/Catering/Housekeeping Work orders. Cannot delete!','The item is linked with a Inventory/Catering/Housekeeping work orders. Cannot delete!','E',6,'U')

Insert into Err_List_S values(636,'Please associate at least one Menu with this Provider.','>Please associate at least one Menu with this Provider.','E',6,'U')


--FB 2599 - Vidyo - 21stFeb 2013
Insert into Err_List_S values(637,'License Check FAILED:Please reduce organizations limit count to 1.','License Check FAILED:Please reduce Max.Organizations count to 1.','E',6,'U')

Insert into Err_List_S values(638,'License Check FAILED:Cloud should be 1 or 0.','License Check FAILED:Cloud should be 1 or 0.','E',6,'U')

--FB 2594 - WhyGo - 21stFeb 2013
Insert into Err_List_S values(639,'License Check FAILED:Public Room Service should be 1 or 0.','License Check FAILED:Public Room Service should be 1 or 0.','E',6,'U')

Insert into Err_List_S values(640,'License Check FAILED:Public Rooms are linked with conference.','License Check FAILED:Public Rooms are linked with conference.','E',6,'U')

Insert into Err_List_S values(641,'License Check FAILED:Public Endpoint is link with Private Room.','License Check FAILED:Public Endpoint is link with Private Room.','E',6,'U')

Insert into Err_List_S values(642,'Public Rooms are linked with conference.Cannot remove!','Public Rooms are linked with conference.Cannot remove!','E',6,'U')

Insert into Err_List_S values(643,'Public Endpoint is link with Private Room.Cannot remove!','Public Endpoint is link with Private Room.Cannot remove!','E',6,'U')

--FB 2586 & FB 2531- VMR room ENHANCEMENT & Issue in setting License- Start23stFeb 2013

Insert into Err_List_S values(655,'VMR Rooms limit exceeds VRM license.','VMR Rooms limit exceeds VRM license.','E',6,'U')

Insert into Err_List_S values(656,'VMR rooms limit exceeded.','VMR rooms limit exceeded.','E',6,'U')

Insert into Err_List_S values(657,'Please deactivate VMR rooms to reduce the count.As there are more active VMR rooms.','Please deactivate VMR rooms to reduce the count.As there are more active VMR rooms.','E',6,'U')


--- validation for video room limits--
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (653,'License Check FAILED: Increase video room limit','License Check FAILED : Increase video room limit','E',6,'U')
 
--- validation for non-video room limits-- 
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (650,'License Check FAILED: Increase non-video room limit','License Check FAILED : Increase non-video room limit','E',6,'U')

--validation for vmr rooms limit---

insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
(658,'License Check FAILED: Increase VMR rooms limit','License check FAILED : Increase VMR rooms limit','E',6,'U')
  
 ---- validation for user limits---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (651,'License Check FAILED : Increase user limit', 
 'License Check FAILED : Increase user limit','E',6,'U')
 
 ---- validation for MCU limits---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (652,'License Check FAILED : Increase MCU limit','License Check FAILED : Increase MCU limit','E',6,'U')
  
 
 ---- validation for Guest room limit ----
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (654,'License Check FAILED : Increase guest room limit','License Check FAILED : Increase guest room limit','E',6,'U')
 

 ---- validation for Enhanced MCU Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (659,'License Check FAILED : Increase enhanced MCU limit','License Check FAILED : Increase enhanced MCU Limit','E',6,'U')
 

 ---- validation for GuestRoomLimit Per user Exceeds ----
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (660,'License Check FAILED : Increase GuestRoomLimit Per User Limit','License Check FAILED : Increase GuestRoomLimit Per User Limit','E',6,'U')
 

 ---- validation for Endpoint Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (661,'License Check FAILED : Increase endpoint limit','License Check FAILED : Increase endpoint limit','E',6,'U')
 
 ---- validation for Outlook User Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (662,'License Check FAILED : Increase Outlook User Limit','License Check FAILED : Increase Outlook User Limit','E',6,'U')
 
 ---- validation for Notes User Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (663,'License Check FAILED : Increase Notes User Limit','License Check FAILED : Increase Notes User Limit','E',6,'U')
 
 ---- validation for Mobile User Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (664,'License Check FAILED : Increase Mobile User Limit','License Check FAILED : Increase Mobile User Limit','E',6,'U')
 
---- validation for Total all User Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (644,'License Check FAILED : Increase all user limit','License Check FAILED : Increase all user limit','E',6,'U')
 
 ---- validation for Facility Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (645,'License Check FAILED : Facility Module is in use.Please increase count',
 'License Check FAILED : Facility Module is in use.Please increase count','E',6,'U')
 
 
---- validation for House Keeping Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (646,'License Check FAILED : Facility Services Module is in use.Please increase count',
 'License Check FAILED : Facility Services Module is in use.Please increase count','E',6,'U')
 
 ---- validation for Catering Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (647,'License Check FAILED : Catering Module is in use.Please increase count',
 'License Check FAILED : Catering Module is in use.Please increase count','E',6,'U')
 
  ---- validation for API Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (648,'License Check FAILED : API module is in use.Please increase count',
 'License Check FAILED : API module is in use.Please increase count','E',6,'U')
 
   ---- validation for PC Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (649,'License Check FAILED : PC module is in use.Please increase count',
 'License Check FAILED : PC module is in use.please increase count','E',6,'U')

--FB 2586 & FB 2531- VMR room ENHANCEMENT & Issue in setting License- End23stFeb 2013

--FB 2636
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(666,N'No E.164 Service found.Please add alteast one E.164 Service.',N'No E164 Service.Please add alteast one E164 Service.','E',6,'U')

--FB 2441
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(665,'MCU is linked with conference(s).It cannot be edited.','MCU is linked with conference(s).It cannot be edited.','E',6,'U')


-- FB 2634
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(611,N'Invalid set-up/start time. Please enter a valid conference start date/time.'
,N'Invalid set-up/start time. Please enter a valid conference start date/time.'
,'E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(612,N'Maximum limit is 24 hours including buffer period. Please enter a valid duration.'
,N'Maximum limit is 24 hours including buffer period. Please enter a valid duration.'
,'E',6,'U')

-- FB 2678
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(667,'Organization ExpiryDate check failed. Please contact your local administrator or myVRM for further assistance.','Organization ExpiryDate check failed. Please contact your local administrator or myVRM for further assistance. ','E',6,'U')

--FB 2653
Insert into Err_List_S (ID,  Message, DescrIPtion, Level, Languageid,Errortype) values 
(668,'MCU IP address has been duplicated.','MCU IP address has been duplicated.','S',6,'U')

Insert into Err_List_S (ID,  Message, DescrIPtion, Level, Languageid,Errortype) values 
(669,'Note: MCU IP address is already in use.','Note: MCU IP address is already in use.','S',6,'U')

--FB 2410
insert into Err_List_S(ID,Message,Description,Level,Languageid,ErrorType)values(671,'The requested job name already exists.Please give another name','The requested job name already exists.Please give another name','S',6,'U')



/************************ FB 2693 10May 2013 **********************************/

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(682,'PC user limit exceeds VRM license.','PC user limit exceeds VRM license.','E',6,'U')

Insert into Err_List_S (ID,  Message, DescrIPtion, Level, Languageid, Errortype) values
(672,'Please delete PC users to reduce the count.As there are more active pc users.','Please delete PC users to reduce the count.As there are more active pc users.','E',6,'U')	

Insert into Err_List_S (ID,  Message, DescrIPtion, Level, Languageid, Errortype) values
(673,'Can not enable Blue Jeans , license count exceeded.','Can not enable Blue Jeans , license count exceeded.','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(674,'Can not enable Jabber , license count exceeded.','Can not enable Jabber , license count exceeded.','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(675,'Can not enable Lync , license count exceeded.','Can not enable Lync , license count exceeded.','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(676,'Can not enable Vidtel , license count exceeded.','Can not enable Vidtel , license count exceeded.','E',6,'U')

Insert into Err_List_S (ID,  Message, DescrIPtion, Level, Languageid, Errortype) values
(677,'License Check FAILED : Blue Jeans is in use.Please increase count','License Check FAILED : Blue Jeans is in use.Please increase count','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(678,'License Check FAILED : Jabber is in use.Please increase count','License Check FAILED : Jabber is in use.Please increase count','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(679,'License Check FAILED : Lync is in use.Please increase count','License Check FAILED : Lync is in use.Please increase count','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(680,'License Check FAILED : Vidtel is in use.Please increase count','License Check FAILED : Vidtel is in use.Please increase count','E',6,'U')

Update Err_List_S set Message = 'User limit should be inclusive for domino,exchange ,mobile and PC users.', Description='User limit should be inclusive for domino,exchange ,mobile and PC users.' where ID = 462 and Languageid = 6

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(681,'Error in inserting User PC records.','Error in inserting User PC records.','E',6,'S')

Insert into Err_List_S (ID,  Message, DescrIPtion, Level, Languageid, Errortype) values
(683,'License Check FAILED : PC User is in use.Please increase count','License Check FAILED : PC User is in use.Please increase count','E',6,'U')

/************************ FB 2693 10May 2013 **********************************/

/************************ FB 2441 II 13May 2013 **********************************/
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(684,N'Password length should be 6 digits for synchronous MCU',N'Password length should be 6 digits for synchronous MCU','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(685,'Conference could not contain both Synchronous and Asynchronous MCU. Please change MCU','Conference could not contain both Synchronous and Asynchronous MCU. Please change MCU','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(686,'Synchronous MCU requires atleast two endpoints to schedule a conference','Synchronous MCU requires atleast two endpoints to schedule a conference','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(687,'Please select same synchronous MCU types to schedule a conference','Please select same synchronous MCU types to schedule a conference','E',6,'U')
/************************ FB 2441 II 13May 2013 **********************************/


/* ************************ FB 2694 - Starts (17 May 2013)************************* */
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(692,'Please deactivate RO Hotdesking rooms to reduce the count.As there are more active RO Hotdesking rooms.','Please deactivate RO Hotdesking rooms to reduce the count.As there are more active RO Hotdesking rooms.','E',6,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values 
(693,'VC Hotdesking rooms limit exceeds VRM license.','VC Hotdesking limit exceeds VRM license.','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values 
(694,'RO Hotdesking rooms limit exceeds VRM license.','RO Hotdesking limit exceeds VRM license.','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(695,'Please deactivate VC Hotdesking rooms to reduce the count.As there are more active VC Hotdesking rooms.','Please deactivate VC Hotdesking rooms to reduce the count.As there are more active VC Hotdesking rooms.','E',6,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(688,'License Check FAILED: Increase Hotdesking video room limit.','License Check FAILED: Increase Hotdesking video room limit.','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(689,'License Check FAILED: Increase Hotdesking non-video room limit.','License Check FAILED: Increase Hotdesking non-video room limit.','E',6,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(690,'One or more selected hotdesking rooms have no audio-video endpoint associated.','One or more selected hotdesking rooms have no audio-video endpoint associated.','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(691,'Only hotdesking rooms are allowed for this conference type.','Only hotdesking rooms are allowed for this conference type.','E',6,'U')

/* ************************ FB 2694 - End (17 May 2013)************************* */

--FB 2670
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (697, 'User is linked with a conference either as VNOC Operator or assigning administrator for VNOC Operator .Cannot delete!',
'User is linked with a conference either as VNOC Operator or assigning administrator for VNOC Operator .Cannot delete!','E',6,'U')

Update Err_List_S set ErrorType='S' where ID in(200,233,238,325,416,422,431,435,439,440,441,442,443,447,448,449,450,472,480,484,500,502,508,509,517,531,547,548,551,552,553,554,555,556)



/************************ FB 2593-MenuPart(26thJune2013-V2.9) Starts **********************************/

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(700,'License Check FAILED : Advanced Report is in use. Please increase count','License Check FAILED : Advanced Report is in use. Please increase count','E',6,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(701,'Can not enable Advanced Report, license count exceeded.','Can not enable Advanced Report, license count exceeded.','E',6,'U')

/************************ FB 2593-MenuPart(26thJune2013-V2.9) End **********************************/



/************************ FB 2870- (15thJuly2013-V2.9) Start **********************************/

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(702,'CTS Numeric ID is Required.','CTS Numeric ID is Required.','E',1,'U')

/************************ FB 2870- (15thJuly2013-V2.9) End **********************************/

/************************ FB 2457- (3rdSep2013-V2.9.2.5) Start **********************************/

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values 
(703, 'Conference was not created Successfully.', 'Conference was not created Successfully.','E',1,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values 
(704, 'Please provide valid URL for connecting exchange service.', 'Please provide valid URL for connecting exchange service.','E',1,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values 
(705, 'Exchange Version Issue.', 'Exchange Version Issue.','E',1,'U')


/************************ FB 2870- (3rdSep2013-V2.9.2.5) Start **********************************/