﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls; 
using System.Text;
using System.Xml.Schema;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using System.IO;
using System.Collections.Specialized;

public partial class en_PasswordChange : System.Web.UI.Page
{

    #region Protected Data members
    protected System.Web.UI.WebControls.Label LblError;
    protected System.Web.UI.WebControls.Label LblMessage;
    protected System.Web.UI.WebControls.Label lblUserName;
    protected System.Web.UI.WebControls.Label lblEmail;
    protected System.Web.UI.WebControls.TextBox txtComment;
    protected System.Web.UI.WebControls.TextBox txtNewPwd;
    protected System.Web.UI.HtmlControls.HtmlButton btnSubmit;
    protected System.Web.UI.HtmlControls.HtmlForm frmEmailLogin;
    protected System.Web.UI.WebControls.HiddenField txtUserID;
    protected System.Web.UI.HtmlControls.HtmlTableRow tdMsg;
    protected System.Web.UI.HtmlControls.HtmlTableRow tdPWD;
    protected System.Web.UI.HtmlControls.HtmlTableRow tdPWD1;
    protected System.Web.UI.HtmlControls.HtmlTableRow trUser;
    protected System.Web.UI.HtmlControls.HtmlTableRow trAdmin;

    string userID = "", userName = "", userEmail = "", language = "", type = "", ErrorLbl = "", OrgID = "";
    string userTheme = "";
    int orgID = 11;

    private StringBuilder inXML = new StringBuilder();
    myVRMNet.NETFunctions obj;
    MyVRMNet.Util utilObj;
    ns_Logger.Logger log;
    MyVRMNet.LoginManagement loginMgmt;

    string COM_ConfigPath = "C:\\VRMSchemas_v1.8.3\\COMConfig.xml";
    string MyVRMServer_ConfigPath = "C:\\VRMSchemas_v1.8.3\\";


    public en_PasswordChange()
    {
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();
        utilObj = new MyVRMNet.Util();
        loginMgmt = new MyVRMNet.LoginManagement();
    }
    #endregion


    private void Page_Init(object sender, System.EventArgs e)
    {
        Application.Add("COM_ConfigPath", COM_ConfigPath);
        Application.Add("MyVRMServer_ConfigPath", MyVRMServer_ConfigPath);
    }

    private void Page_Load(object sender, System.EventArgs e)
    {
        try
        {
            if (loginMgmt == null)
                loginMgmt = new MyVRMNet.LoginManagement();
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("PasswordChange.aspx", Request.Url.AbsoluteUri.ToLower());

            string path = Request.Url.AbsoluteUri.ToLower();
            if (path.IndexOf("%3c") > -1 || path.IndexOf("%3e") > -1)
                Response.Redirect("ShowError.aspx");

            //if (Session["language"] == null)
            //    Session["language"] = "en";

            //if (Session["language"].ToString() != "")
            //    language = Session["language"].ToString();

            if (Request.QueryString["id"] != null)
            {
                userID = Request.QueryString["id"];
                loginMgmt.simpleDecrypt(ref userID);
                Session["userID"] = userID;
            }

            this.CheckUserSession();
           
            if (Request.QueryString["un"] != null)
            {
                userName = Request.QueryString["un"];
                loginMgmt.simpleDecrypt(ref userName);
            }

            if (Request.QueryString["ei"] != null)
            {
                userEmail = Request.QueryString["ei"];
                loginMgmt.simpleDecrypt(ref userEmail);
            }
            if (Request.QueryString["ty"] != null)
                type = Request.QueryString["ty"];
            string[] userLang = type.Split(',');
            type = userLang[0];
            Session["languageID"] = userLang[1];
            if (Request.QueryString["o"] != null)
            {
                OrgID = Request.QueryString["o"];
                loginMgmt.simpleDecrypt(ref OrgID);
                int.TryParse(OrgID, out orgID);
            }
            Session["organizationID"] = orgID;
            if (Request.QueryString["t"] != null)
            {
                userTheme = Request.QueryString["t"];
                loginMgmt.simpleDecrypt(ref userTheme);
                Session["ThemeType"] = userTheme;
            }
            string inxml = "<login>" + obj.OrgXMLElement() + "<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID></login>";
            string outXML = obj.CallCommand("GetLanguageTexts", inxml);

            if (HttpContext.Current.Session["TranslationText"] == null)
                HttpContext.Current.Session.Add("TranslationText", outXML);
            else
                HttpContext.Current.Session["TranslationText"] = outXML;
            if (type == "U")
            {
                tdMsg.Visible = true;
                tdPWD.Visible = false;
                tdPWD1.Visible = false;
                trAdmin.Visible = false;
                trUser.Visible = true;
                ErrorLbl = obj.GetTranslatedText("You will receive the password shortly. Administrator(s) are notified about your request.");
            }
            else
            {
                tdPWD.Visible = true;
                tdPWD1.Visible = true;
                tdMsg.Visible = false;
                trAdmin.Visible = true;
                trUser.Visible = false;
                ErrorLbl = obj.GetTranslatedText("User password has been changed and email notification has been sent to user.");
            }

            if (!IsPostBack)
            {

                if (userID != "" && userName != "" && userEmail != "")
                {
                    txtUserID.Value = userID;
                    lblUserName.Text = userName;
                    lblEmail.Text = userEmail;
                }
                else
                {
                    //errLabel.Text = obj.ShowErrorMessage(outXML);
                    //trButtonsHeader.Visible = false;
                    //trButtons.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            //errLabel.Visible = true;
            //log.Trace("Page_Load" + ex.Message);
            //errLabel.Text = obj.ShowSystemMessage();
        }

    }


    #region CheckUserSession
    // <summary>
    /// Checks the User Session and sets application path.
    /// </summary>

    private void CheckUserSession()
    {
        if (Session["userID"] == null)
        {
            Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
            Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
        }
    }

    #endregion

    #region SendRequrst
    /// <summary>
    /// BtnSubmit_Click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void SendRequrst(object sender, EventArgs e)
    {
        try
        {
            BuildInXML();
            GetOutXML(inXML);
        }
        catch (System.Threading.ThreadAbortException)
        {

        }
        catch (Exception ex)
        {
            log.Trace("Submitting" + ex.Message);
            LblError.Text = obj.ShowSystemMessage();
            LblError.Visible = true;
        }

    }

    #endregion

    #region User Defined Methods
    /// <summary>
    /// User Defined Methods
    /// </summary>

    #region BuildInXML
    private void BuildInXML()
    {
        inXML = new StringBuilder();
        inXML.Append("<PasswordChangeRequest>");
        if (Session["organizationID"] == null)
            Session["organizationID"] = "11";
        inXML.Append(obj.OrgXMLElement());
        inXML.Append("<sendRequestInfo>");
        inXML.Append("<userID>" + txtUserID.Value.ToString() + "</userID>");
        inXML.Append("<userComment>" + txtComment.Text.ToString() + "</userComment>");
        inXML.Append("<type>" + type + "</type>");
        if (type != "U" && !string.IsNullOrEmpty(txtNewPwd.Text))
            inXML.Append("<password>" + obj.PasswotdEncrpyt(txtNewPwd.Text) + "</password>");
        inXML.Append("</sendRequestInfo>");
        inXML.Append("</PasswordChangeRequest>");
    }

    #endregion

    #region GetOutXML
    /// <summary>
    /// GetOutXML
    /// </summary>
    /// <param name="inXML"></param>

    private void GetOutXML(StringBuilder inXML)
    {
        string outXML = obj.CallCommand("PasswordChangeRequest", inXML.ToString());

        if (outXML.IndexOf("<error>") < 0)
        {
            btnSubmit.Disabled = true;
            btnSubmit.Attributes.Add("Class", "btndisable"); 
            LblMessage.Text = obj.GetTranslatedText(ErrorLbl);
            LblMessage.Attributes.Add("Style", "Color:Green");
            txtComment.Text = "";
            txtNewPwd.Text = "";
        }
        else if (outXML.IndexOf("<error>-1</error>") >= 0)
            Response.Redirect("genlogin.aspx");
    }
    #endregion

    #region SubmitAction

    protected void SubmitAction(Object sender, EventArgs e)
    {
        try
        {
            BuildInXML();
            GetOutXML(inXML);
        }
        catch (System.Threading.ThreadAbortException)
        {

        }
        catch (Exception ex)
        {
            log.Trace("Submitting" + ex.Message);
            LblError.Text = obj.ShowSystemMessage();
            LblError.Visible = true;
        }
    }

    #endregion

    #endregion
}
